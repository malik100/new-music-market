﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MMCustomerVersion
{
    /// <summary>
    /// Interaction logic for WizardWindows.xaml
    /// </summary>
    public partial class WizardWindows : Window
    {
        public WizardWindows()
        {
            InitializeComponent();
        }
        // intro page

        private void tbPassword_wiz_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbPassword_wiz.Text)) // to verify password
            {
                IntroPage.CanSelectNextPage = false;
            }
            else
            {
                IntroPage.CanSelectNextPage = true;
            }
        }

        // page 0

        private void btAdd_wiz_Click(object sender, RoutedEventArgs e)
        {
            // to add more songs to the list

        }

        private void btRemove_wiz_Click(object sender, RoutedEventArgs e)
        {
            // to remove songs from the current list

        }

        private void btConfirm_wiz_Click(object sender, RoutedEventArgs e)
        {
            // to confirm selection

            Page0.CanSelectNextPage = true;
        }

        // page 1

        private void btConfirmFinal_wiz_Click(object sender, RoutedEventArgs e)
        {
            // to confirm final selection

            Page1.CanSelectNextPage = true;
        }

        // page 2

        private void btVerifyPayment_Click(object sender, RoutedEventArgs e)
        {


            Page2.CanSelectNextPage = true;
        }

        // last page

    }
}

use master
;
go 



/*--------------------------------------------------------------*/
create database MusicDB2
on primary
(
-- 1) rows data logical file name
	name = 'MusicDB',
-- 2) rows data initial file size
	size = 12MB,
-- 3) rows data auto growth size
	filegrowth = 10MB,
-- 4) rows data maximum file size
	maxsize = 500MB, -- or unlimited
-- 5) rows data path and file name
	filename = 'your directory'
)
log on
(
	-- 1) log logical file logical name
	name = 'MusicDB2',
	-- 2) log initial file size (1/4 pf data file size)
	size = 3MB,
	-- 3) log auto growth size
	filegrowth = 10%,
	-- 4) log maximum file size
	maxsize = 25MB,
	-- 5) log path file name
	filename = 'your directory'
)
;
go
/*--------------------------------------------------------------*/

use MusicDB2;
go

create table Songs
(
	Id int identity not null primary key,
	SongPrice decimal(5,2) not null ,
	SongName nvarchar(60) not null,
	Genre nvarchar(30) not null,
	ArtistId int not null,
	AlbumId int null
)
;
go

create table Albums
(
	Id int identity not null primary key,
	AlbumPrice decimal(5,2) not null ,
	AlbumName nvarchar(60) not null,
	ReleaseDate DateTime not null,
	ArtistId int not null,
	TransactionId int not null, --FK
	CustomerId int not null --FK
)
;
go

create table Artists	--COMPLETE
(
	Id int identity not null primary key,
	ArtistName nvarchar(60) not null
)
;
go

create table PaymentInfo
(
	Id int identity not null primary key,
	CardholderName nvarchar(60) not null,
	ZipCode nvarchar(6) not null,
	ExpirationDate DateTime not null,
	CardNumber int not null,
	CustomerId int not null, --FK
)
;
go
create table Transactions
(
	Id int identity not null primary key,
	TransactionDate DateTime not null,
	CustomerId int not null, --FK
	PaymentInfoId int not null, --FK
)
;
go
--TABLE TRANSACTIONS-ALBUMS
create table TransactionAlbums
(
	TransactionId int not null, --FK PK-composite
	AlbumId int not null --FK PK-composite
)
;
go
--TABLE TRANSACTIONS SONGS
create table TransactionSongs
(
	TransactionId int not null, --FK PK-composite
	SongId int not null --FK PK-composite
)
;
go
--////////////////////////////////////////////////
create table Customers
(
	Id int identity not null primary key,
	CustomerName nvarchar(60) not null,
)
;
go
--TABLE CUSTOMER-ALBUMS--------------------------------------
create table ListeningRecord
(
	CustomerId int not null, --FK PK-composite
	AlbumId int not null --FK PK-composite
)
;
go
--TABLE CUSTOMER-SONGS---------------------------------------
create table ListeningRecord
(
	CustomerId int not null, --FK PK-composite
	SongId int not null --FK PK-composite
)
;
go
--TABLE LISTENINGRECORDS-------------------------------------
create table ListeningRecord
(
	Id int identity not null primary key,
	DateOf DateTime not null,
	CustomerId int not null, --FK
	SongId int not null --FK
)
;
go
--TABLE CUSTOMER-LISTENINGRECORDS-----------------------------
create table CustomerListeningRecord
(
	CustomerId int not null, --FK & PK-composite
	ListeningRecordId int not null --FK PK-composite
)
;
go
--////////////////////////////////////////////////////////////


alter table Songs
add constraint fk_Songs_Artists foreign key (ArtistId)
references Artists (Id)
;
go

alter table Songs
add constraint fk_Songs_Albums foreign key (AlbumId)
references Albums (Id)
;
go

alter table Albums
add constraint fk_Albums_Artists foreign key (ArtistId)
references Artists (Id)
;
go
	
	
	
	